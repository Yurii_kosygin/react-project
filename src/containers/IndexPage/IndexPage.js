  /*
 * IndexPage
 *
 * Route: /
 *
 */
import React, { Component, PropTypes as Type } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { IndexActions, FriendsAction, FriendsIdActions, MessageActions } from '../../actions'
import CSSModules from 'react-css-modules'
import styles from './IndexPage.css'
import { ListFr } from '../../components'
import { MessageBlock } from '../../components'
import { InputMessage } from '../../components'
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import IconFriend from 'material-ui/svg-icons/social/group';
import IconChat from 'material-ui/svg-icons/communication/chat-bubble-outline';

const friendsIcon = <IconFriend className="material-icons"></IconFriend>;
const chatIcon = <IconChat className="material-icons"></IconChat>;

@connect(state => ({
  page: state.page,
  friends: state.friends,
  idfriend: state.idfriend,
  message: state.message
}), dispatch => ({
  dispatch: dispatch,
  indexActions: bindActionCreators(IndexActions, dispatch),
  friendsActions: bindActionCreators(FriendsAction, dispatch),
  friendsIdActions: bindActionCreators(FriendsIdActions, dispatch),
  messageActions: bindActionCreators(MessageActions, dispatch)
}))

@CSSModules(styles, {allowMultiple: true})
export default class IndexPage extends Component {

  state = {
    selectedIndex: 0,
  };

  select = (index) => this.setState({selectedIndex: index});

  static propTypes = {
    dispatch: Type.func.isRequired
  };

  constructor(props, context) {
    super(props, context)
  }

  _onClick() {
    this.props.indexActions.updateDataAction()
    this.props.friendsActions.updateFriendsList()
  }

  _getIdFriend(friend) {

     this.props.friendsIdActions.getFriendId(friend)
  }

  _hendlerSendMessage() {
    this.props.messageActions.sendMessage()
  }

  render() {
    const { page, friends, idfriend, message } = this.props
    const userName = page.text
    const friendsList = friends.friendsList
    const friendId = idfriend.friend
    return (
      <div styleName='root'>
        <header>
          <button 
            onClick={this._onClick.bind(this)}>
            Login VK
          </button>
          <span>{userName}</span>
        </header>
        <ul>
          { friendsList.map((friend, key) =>(
            <li 
              key={key}
              onClick = {this._getIdFriend.bind(this,friend.uid)}
            >
            <ListFr friend = {friend}/></li>
            ))}
        </ul>
        <MessageBlock />
        <div styleName='bottomBtn'>
          <BottomNavigation selectedIndex={this.state.selectedIndex}>
            <BottomNavigationItem
              icon={friendsIcon}
              onTouchTap={() => this.select(0)}
            />
            <BottomNavigationItem
              icon={chatIcon}
              onTouchTap={() => this.select(1)}
            />
          </BottomNavigation>
        </div>
        <InputMessage
          getIdFriend = {friendId}
          onSend={::this._hendlerSendMessage}
        />
        <footer></footer>
        <a href="https://oauth.vk.com/authorize?client_id=5730379&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=offline,messages&response_type=token&v=5.37">Токен</a>
      </div>  
    )
  }
}
