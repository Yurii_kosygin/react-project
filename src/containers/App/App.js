/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

// Import stuff
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators} from 'redux'

import * as actions from '../../actions'
import CSSModules from 'react-css-modules'
import styles from './App.css'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

@CSSModules(styles, {allowMultiple: true})
class App extends Component {

  render() {
    return (
    	<MuiThemeProvider>
	      <div styleName="app">
	        { this.props.children }
	      </div>
      	</MuiThemeProvider>
    )
  }
}

// export default App

// REDUX STUFF
// Which props do we want to inject, given the global state?
const mapStateToProps = (state) => (state)

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actions, dispatch)
})

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(App)
