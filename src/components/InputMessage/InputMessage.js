import React, { Component, PropTypes as Type } from 'react'
import CSSModules from 'react-css-modules'
import styles from './InputMessage.css'

@CSSModules(styles, {allowMultiple: true})

export default class InputMessage extends Component {

	constructor(props) {
		super(props);
    this.state = {
      inputVal: ""
    }

	}
  _inputMessage(e) {
    this.setState({
      inputVal: e.target.value
    });
  }
  _sendMessage(getIdFriend) {
    // const token = "94f661e3615cc1ce59577558b43b0b22a4ea137b9db4fec969184823a2877b9f4bed19e0936d7a37dbc2d"
    // const message = this.state.inputVal
    // const userId = getIdFriend
    // const win = window.open("https://api.vk.com/method/messages.send?user_id="+userId+"&message="+message+"&v=5.37&access_token="+token)
    // this.setState({
    //   inputVal: ""
    // });
    this.props.onSend()

  }

    render() {
    	const friend = this.props.friend
      const getIdFriend = this.props.getIdFriend
    	return (
      		<div styleName='root-input-message'>
            <input
              placeholder="Message"
              type = "text"
              onChange = {::this._inputMessage}
              value = {this.state.inputVal}
            />
            <button
              onClick = {::this._sendMessage}
            >
              Send
            </button>
      		</div>
    	)
  	}	
}