import React, { Component, PropTypes as Type } from 'react'
import CSSModules from 'react-css-modules'
import styles from './ListFr.css'

@CSSModules(styles, {allowMultiple: true})

export default class ListFr extends Component {

	constructor(props) {
		super(props);

	}
    render() {
    	
    	const friend = this.props.friend
    	return (
      		<div styleName='root-friends'>
      			<div styleName="friend-avatar">
      				<img alt='' src={friend.photo}/>
      			</div>
      			<div styleName="friend-name">

      				<div styleName={"friend-status " + (friend.online == 0 ? "friend-offline" : "friend-online")}></div>
      				<span>{friend.first_name} {friend.last_name}</span>
      				<span>id : {friend.uid}</span>
      			</div>
      		</div>
    	)
  	}	
}