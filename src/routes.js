import React from 'react'
import { Route, IndexRoute } from 'react-router'

// Import the containers used as pages
import {
  App,
  IndexPage,
} from './containers'

export default (authAgent) => {

  return (
    <Route path="/" component={App}>
      <IndexRoute component={IndexPage}/>
    </Route>
  )
}
