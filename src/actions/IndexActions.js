import * as actionTypes from '../constants/IndexConstants'

export function updateDataAction(text) {
  return (dispatch, getState) => {
    dispatch({ type: actionTypes.LOGIN_REQUEST })

    VK.Auth.login((r) => { 
      if (r.session) {
        let username = r.session.user.first_name + " " + r.session.user.last_name;
        text = username

        dispatch({
          type: actionTypes.LOGIN_SUCCES,
          text
        })

      } else {
        dispatch({
          type: actionTypes.LOGIN_FAIL,
          error: true
        })
      }
    },4);
  }
}


