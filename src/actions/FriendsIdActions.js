import * as actionTypes from '../constants/FriendsIdConstants'

export function getFriendId(friend) {
  return (dispatch, getState) => {
    dispatch({ 
      type: actionTypes.GET_FRIENDS_ID,
      friend
    })
    
  }
}


