import * as actionTypes from '../constants/FriendsConstants'

export function updateFriendsList() {
	return (dispatch, getState) => {
		VK.api("friends.get", {fields:"uid", fields:"photo"}, function (friends) {
        const fr = friends.response.length;
        const friends_data = friends.response.sort(sFirstName);
        dispatch({ 
        	type: actionTypes.GET_FRIENDS_SUCCESS,
        	friendsList: friends_data
         })
      });
	}
}
function sFirstName(a,b) {
  if (a.first_name > b.first_name)
    return 1;
  else if  (a.first_name < b.first_name)
    return -1;
  else
    return 0;
}