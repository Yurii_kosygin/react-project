import * as actionTypes from '../constants/FriendsIdConstants'

const initialState = {
  friend: 0
}

export default function idfriend(state = initialState, action) {
  const { type, friend } = action

  switch (type) {
    case actionTypes.GET_FRIENDS_ID:
      return {
        friend
      }
    default:
      return state
  }
}