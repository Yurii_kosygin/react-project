import * as actionTypes from '../constants/FriendsConstants'

const initialState = {
  friendsList: []
}

export default function friends(state = initialState, action) {
  const { type, friendsList } = action

  switch (type) {
    case actionTypes.GET_FRIENDS_REQUEST:
      return {
        friendsList
      }
    case actionTypes.GET_FRIENDS_SUCCESS:
      return {
        friendsList
      }
    case actionTypes.GET_FRIENDS_FAIL:
      return {
        friendsList
      }

    default:
      return state
  }
}