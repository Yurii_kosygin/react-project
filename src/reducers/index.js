/*
 * The reducer takes care of our data
 * Using actions, we can change our application state
 * To add a new action, add it to the switch statement in the homeReducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return {
 *     ...state,
 *     stateVariable: action.var
 *   }
 */

import { combineReducers } from 'redux'
import page from './page'
import friends from './friends'
import idfriend from './idfriend'
import message from './message'

const rootReducer = combineReducers({
  page,friends,idfriend, message
})

export default rootReducer
