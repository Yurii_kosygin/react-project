import * as actionTypes from '../constants/IndexConstants'

const initialState = {
  text: ''
}

export default function page(state = initialState, action) {
  const { type, text } = action

  switch (type) {
    case actionTypes.LOGIN_REQUEST:
      return {
        text
      }
    case actionTypes.LOGIN_SUCCES:
      return {
        text
      }
    case actionTypes.LOGIN_FAIL:
      return {
        text
      }

    default:
      return state
  }
}
