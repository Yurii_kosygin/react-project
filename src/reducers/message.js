import * as actionTypes from '../constants/MessageConstants'

const initialState = {
  message: ""
}

export default function message(state = initialState, action) {
  const { type, message } = action

  switch (type) {
    case actionTypes.MESSAGE_SEND:
      return {
        message
      }
    default:
      return state
  }
}