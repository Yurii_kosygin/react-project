var express = require('express');
var app = express();
var request = require('request');

// Add headers
app.use(function (req, res, next) {

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
	res.setHeader('Access-Control-Allow-Origin', '*');
	// res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    // Pass to next layer of middleware
    next();
});
// Routes

app.get('/', function(req, res){
  res.send('hello world');
});

app.get('/messages-send', function(req, res) {
	request('https://api.vk.com/method/messages.send?user_id=45432742&message=%D0%BF%D1%80%D0%B8%D0%B2%D0%B5%D1%82&v=5.37&access_token=94f661e3615cc1ce59577558b43b0b22a4ea137b9db4fec969184823a2877b9f4bed19e0936d7a37dbc2d', function (error, response, body) {
	  if (!error && response.statusCode == 200) {
	    console.log(body) // Print the google web page.
	  }
	})
  res.send({ message: 'hello world' });

});

app.listen(3999);