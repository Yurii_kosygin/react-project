module.exports = [
  require('postcss-import'),
  require('autoprefixer')({
    browsers: [
      'last 2 versions',
      'ie 11'
    ]
  }),
  require('postcss-simple-vars')({
    variables: {
      font_basic: 'system, -apple-system, Helvetica, Arial, sans-serif',
      black: '#000000',
      white: '#ffffff',
      red: '#ce0000'
    }
  }),
  require('postcss-nested'),
  require('postcss-easings'),
  require('postcss-color-function'),
  require('postcss-flexbugs-fixes'),
  require('postcss-autoreset')({
    reset: {
      boxSizing: 'border-box'
    }
  }),
  require('postcss-mixins')({
    mixins: {
      typo: {
        color: '#000000',
        'font-family': 'system, -apple-system, Helvetica, Arial, sans-serif',
        'font-size': '14px',
        'letter-spacing': '0.025em'
      }
    }
  }),
  // This plugin makes sure we get warnings in the console
  require('postcss-reporter')({
    clearMessages: true
  })
]
