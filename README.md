# React project start

## Getting started

1. Clone this repo using `git clone git@bitbucket.org:manufactura/react-project-start.git`.

2. Run `npm install` to install the dependencies.

3. Run `npm start` to start the local web server.

4. Go to `http://localhost:3000` and you should see the app running!

[**Use a rigid commit message format**](https://seesparkbox.com/foundry/semantic_commit_messages)